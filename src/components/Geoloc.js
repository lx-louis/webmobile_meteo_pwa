import React from "react";
import Geolocation from "react-geolocation";

const Geoloc = () => {
    return (
        <Geolocation
        render={({ getCurrentPosition, fetchingPosition, position }) => (
          <div>
            <button onClick={getCurrentPosition}>Get Current Position</button>
            <p>Fetching Position: {fetchingPosition ? "YES" : "NO"}</p>
            <p>{position && position.coords.latitude}</p>
            <p>{position && position.coords.longitude}</p>
          </div>
        )}
      />
    ) 
};

export default Geoloc;